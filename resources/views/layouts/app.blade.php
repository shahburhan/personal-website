<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
        .navbar-brand{
            font-family: Arizonia;
            font-size: 32px;
        }
        .macbook {
    position: relative;
}
:not(pre) > code[class*="language-"], pre[class*="language-"] {
    background: rgba(238, 238, 238, 0.35);
    border-radius: 3px;
    padding: 10px;
    -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.125);
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.125);
}
pre.line-numbers {
    position: relative;
    padding-left: 3.8em;
    padding-top: 0px;
    margin-top: -1px;
    border-radius: 0;
    counter-reset: linenumber;
    margin-bottom: 0px;
}

.line-numbers .line-numbers-rows {
    position: absolute;
    pointer-events: none;
    top: -2px;
    padding-top: 0px;
    font-size: 100%;
    left: -3.8em;
    width: 3em;
    letter-spacing: -1px;
    background: #f0f2f1;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.line-numbers-rows > span:before {
    counter-increment: linenumber;
    content: counter(linenumber);
    color: #999;
    display: block;
    padding-right: 0.8em;
    text-align: right;
}
pre.line-numbers > code {
    position: relative;
}
.token.atrule, .token.attr-value, .token.keyword {
    color: #07a;
}
.token.punctuation {
    color: #999;
}
.token.comment, .token.prolog, .token.doctype, .token.cdata {
    color: #999;
}
code[class*="language-"], pre[class*="language-"] {
    color: black;
    text-shadow: 0 1px white;
    font-family: "Operator Mono", "Fira Code", Consolas, Monaco, "Andale Mono", monospace;
    direction: ltr;
    text-align: left;
    white-space: pre;
    word-spacing: normal;
    word-break: normal;
    line-height: 1.7;
    font-size: 12px;
    -moz-tab-size: 4;
    -o-tab-size: 4;
    tab-size: 4;
    -webkit-hyphens: none;
    -ms-hyphens: none;
    hyphens: none;
}
.macbook  pre {
    position: absolute;
    z-index: 2;
    height: 321px;
    top: 46px;
    width: 554px;
    left: 50%;
    margin-left: -278px;
    padding-bottom: 0;
    overflow: hidden;
}
.macbook svg {
    height: 425px;
    width: 100%;
    height: 425px;
    position: relative;
}
.browser-window, .terminal-window {
    text-align: left;
    margin: 20px;
    width: 602px;
    min-height: 415px;
    display: inline-block;
    border-radius: 4px;
    background-color: #fff;
    border: 1px solid #ddd;
    -webkit-box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.1);
    box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.1);
    overflow: overlay;
}
.browser-window .top-bar, .terminal-window .top-bar {
    height: 30px;
    border-radius: 4px 4px 0 0;
    border-top: thin solid #eaeae9;
    border-bottom: thin solid #dfdfde;
    background: #ebebeb;
}
.browser-window .window-content {
    overflow: hidden;
    width: 100%;
    height: 100%;
}
.browser-window {
    display: block;
    width: auto;
    overflow: hidden;
}
.browser-window .circle, .terminal-window .circle {
    height: 8px;
    width: 8px;
    display: inline-block;
    border-radius: 50%;
    background-color: white;
}
.browser-window .circles, .terminal-window .circles {
    margin: 1px 10px;
}
.macbook{
    display:none;
}
@media(max-width: 767px){
    .browser-window{
        display: block;
    }
    .macbook{
        display: none;
    }
}

    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Burhan Shah') }}!
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('about') }}">{{ __('About') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">{{ __('Work') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">{{ __('Blog') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">{{ __('Contact') }}</a>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-0">
            @yield('content')
        </main>
    </div>
</body>
</html>
