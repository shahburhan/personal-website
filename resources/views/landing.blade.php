@extends('layouts.app')
@section('content')
<div class="browser-window">
	<div class="top-bar">
		<div class="circles">
			<div class="circle circle-red"></div>
			<div class="circle circle-yellow"></div>
			<div class="circle circle-green"></div>
		</div>
	</div>
	<div class="window-content">
		<pre class="line-numbers language-php"><code class=" language-php"><span class="token delimiter">&lt;?php</span>

<span class="token">namespace</span> <span class="token keyword">Home</span>;

<span class="token keyword">class</span> <span class="token class-name">Hello</span> <span class="token keyword">extends</span> <span class="token class-name">Welcome</span>
<span class="token punctuation">{</span>
	<span class="token comment" spellcheck="true">/*
    |-----------------------------------<span class="d-none d-sm-inline">---------------------------------------</span>
    | Burhan Shah, New Delhi, IN
    |-----------------------------------<span class="d-none d-sm-inline">---------------------------------------</span>
    |
    | PHP Developer
    | All things Laravel
    | Wordpress Explorer
    | Opencart Expert
    | API Guru
    | 2 Big hearts for Commit and Push
    |
    */</span>
   
<span class="token punctuation">}</span><span class="line-numbers-rows"><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span></code></pre>
	</div>
</div>

@endsection
